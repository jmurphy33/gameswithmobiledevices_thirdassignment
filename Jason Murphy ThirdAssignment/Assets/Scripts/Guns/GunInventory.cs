﻿using UnityEngine;
using System.Collections;

public class GunInventory : MonoBehaviour {
    
    // current weapon player is using
    public GameObject weapon;
    // spawn location for weapon
    public GameObject spawn;
    
    // Use this for initialization
    void Awake () {
       
        spawnWeapon(weapon);    
	}
	
    /// <summary>
    /// Allows the player to switch their current gun for another
    /// </summary>
    /// <param name="gun"></param>
	public void SwitchGun(GameObject gun)
    {
        // destory current weapon
        destroyWeapon();
        // spawn new weapon
        spawnWeapon(gun);
    }

    /// <summary>
    /// Destory the players current gun
    /// </summary>
    void destroyWeapon()
    {
        // destory the current gun immediately in order to spawn new gun
        DestroyImmediate(weapon);
    }

    /// <summary>
    /// Spawns new gun for the player
    /// </summary>
    /// <param name="gun"></param>
    void spawnWeapon(GameObject gun)
    {     
        // sets the current weapon to the passed in gun
        weapon = (GameObject)Instantiate(gun, spawn.transform.position , transform.rotation);
        // set the parent of the weapon to the player game object
        weapon.transform.parent = transform;

    }
}
