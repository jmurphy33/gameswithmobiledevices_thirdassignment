﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunGUI : MonoBehaviour
{
    // reference to the current gun gameobject
    public GameObject weapon;
    // reference to the current gun component
    private Gun gun;
    // name of the gun
    public Text weaponName;
    // price of the gun
    public Text price;
    // description for the gun
    public Text description;

    // Use this for initialization
    void Start()
    {
        // set the gun 
        gun = weapon.GetComponent<Gun>();

        if (gun != null)
        {
            // set the name, price and description for shop
            setWeaponName();
            setPrice();
            setDescription();
        }
    }

    void setWeaponName()
    {
        if (weaponName != null)
        {
            // set gun name text
            weaponName.text = gun.weaponName;
        }
    }

    void setPrice()
    {
        // check if gun is bought using real or in game money
        if(gun.purchaseType == Gun.PurchaseType.inGameMoney)
        {
            // set price text to show is in game money followed by cost
            price.text = "*" + gun.cost;
        }
        else
        {
            // set price text to show is real money followed by cost
            price.text = "€ " + gun.cost;
        }
        
    }

    void setDescription()
    {
        description.text = gun.description;
    }
}
