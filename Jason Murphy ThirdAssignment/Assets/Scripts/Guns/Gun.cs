﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    /// <summary>
    /// Resets the ammo to the orginal ammount
    /// </summary>
    public void Reload()
    {
        ammo = baseAmmo;
    }

    /// <summary>
    /// type of gun being used
    /// </summary>
    public enum Type
    {
        machinegun,
        pistol
    }

    /// <summary>
    /// Type of purchase for gun
    /// </summary>
    public enum PurchaseType
    {
        realMoney,
        inGameMoney
    }
    // purchase type for gun
    public PurchaseType purchaseType;
    // type of gun
    public Type type;
    // name of the gun to display
    public string weaponName = "Weapon name";
    // cost for the gun
    public float cost = 50;
    // description for shop
    public string description;
    // how far the gun can shoot
    public float range = 100;
    // colour of gun effects
    public Color color = Color.yellow;
    // fire rate of the gun
    public float fireRate = .5f;
    // damage to apply to anything hit
    public int damage = 10;
    // current ammo amount
    public int ammo = 30;
    // orginal ammo count 
    private int baseAmmo = 30;
    // rounds per minute to fire (not used)
    public int rpm = 200;
    // where to spawn gun
    public GameObject spawn;
  
}
