﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    // reference to the player
    private GameObject tagret;
    // reference to the player's health
    private HealthController playerHealth;

    NavMeshAgent nav;

    // Use this for initialization
    void Start()
    {
        // set the target to the player
        tagret = GameObject.FindGameObjectWithTag("Player");
        // get the health of the player
        playerHealth = tagret.GetComponent<HealthController>();
        // get the navmesh agent component
        nav = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        // if player is alive
        if (playerHealth.getHealth() >= 0)
        {
            // set the destination to the players location
            nav.SetDestination(tagret.transform.position);
        }   
    }
}
