﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    // player to reference
    GameObject player;
    // health of the player
    HealthController playerHealth;
    // if the player's
    bool playerInRange;
    // damage of the enemy
    int damage = 25;

    // time inbetween attacks
    float timer;
    public float timeBetweenAttacks = 0.5f;


    // Use this for initialization
    void Start()
    {
        // set the player
        player = GameObject.FindGameObjectWithTag("Player");
        // get the player's health component
        playerHealth = player.GetComponent<HealthController>();

    }

    void OnTriggerEnter(Collider other)
    {
        // If the entering collider is the player...
        if (other.gameObject == player)
        {
            // ... the player is in range.
            playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject == player)
        {
            // ... the player is no longer in range.
            playerInRange = false;
        }
    }


    void Attack()
    {   
        // time inbetween attacks
        timer = 0f;  

        // if the player is alive then attack     
        if (playerHealth.getHealth() >= 0)
        {    
            playerHealth.takeDamage(damage);

        }
    }

    // Update is called once per frame
    void Update()
    {
        // set time inbetween attacks
        timer += Time.deltaTime;

        // if time has passed and player is in range of an attack
        if (timer >= timeBetweenAttacks && playerInRange)
        {
            Attack();
        }
    }
}
