﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    // time inbetween spawns
    public float spawnTime = 3f;
    // game object to spawn
    public GameObject enemy;
    // array of spawn locations
    public Transform[] spawnPoints;
    // references the player's health component
    public HealthController playerHealth;

	// Use this for initialization
	void Start () {
        // repeatedly calls enemy spawn
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }
	
	/// <summary>
    /// Spawns enemies
    /// </summary>
    void Spawn()
    {
        // if the player is dead return
        if(playerHealth.getHealth() <= 0)
        {
            return;
        }

        // spawn at one of the spawn locations at random
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // instantiate the enemy game object
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
    
}
