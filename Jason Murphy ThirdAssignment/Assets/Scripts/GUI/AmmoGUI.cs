﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AmmoGUI : MonoBehaviour
{
    // reference to the player
    public GameObject player;
    // reference to the player's current gun
    public Gun gun;
    // keeps track of the player's gun's current ammo
    public Text AmmoText;


    void Start()
    {
        setGun();
    }

    // Update is called once per frame
    void Update()
    {
        if (gun != null)
        {
            // set the text of the ammo to the gun's name and current ammo
            AmmoText.text = gun.weaponName+": "+gun.ammo;
        }
    }

    public void setGun()
    {
        // gets the player's current gun
        gun = player.GetComponentInChildren<Gun>();
    }
}
