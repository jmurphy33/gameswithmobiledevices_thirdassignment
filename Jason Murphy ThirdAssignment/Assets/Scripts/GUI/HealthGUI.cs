﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthGUI : MonoBehaviour {    
    // references the player's health
   public HealthController healthController;
   public Text healthText;
	
	// Update is called once per frame
	void Update () {
        // sets the text to the health of the player
        healthText.text = "HEALTH: "+healthController.getHealth();
	}
}
