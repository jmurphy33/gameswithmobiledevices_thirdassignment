﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour
{
    // references GU Canvas' for controls and shop    
    public Canvas controls;
    public Canvas shop;
  
    void Update()
    {
        // adjusts the orientation of the screen
        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {   
            // enable game controls
            enableControllsCanvas();
            // set the game to unpaused
            Time.timeScale = 1;
        }  
    }

    /// <summary>
    /// Disables the game controls
    /// </summary>
    public void disableControllsCanvas()
    {
        controls.GetComponent<Canvas>().enabled = false;
    }

    /// <summary>
    /// Enables the game controls
    /// </summary>
    public void enableControllsCanvas()
    {
        controls.GetComponent<Canvas>().enabled = true;
    }

    /// <summary>
    /// Disable the shop canvas
    /// </summary>
    public void disableShopCanvas()
    {
        shop.GetComponent<Canvas>().enabled = false;
    }

    /// <summary>
    /// Enable shop canvas
    /// </summary>
    public void enableShopCanvas()
    {
        shop.GetComponent<Canvas>().enabled = true;
    }

 
}

