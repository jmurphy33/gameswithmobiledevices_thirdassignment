﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WalletGUI : MonoBehaviour
{
    public Text walletText;

    void Update()
    {
        // displays the player's current money
        walletText.text = "" + Wallet.money;

    }

}
