﻿using UnityEngine;
using System.Collections;

public class HealthController : MonoBehaviour {

    // keeps track of the health of the current game object
    private int health;
 
    void Awake () {
        // set the health of the game object
        health = 100;     
	}

    /// <summary>
    /// returns the health of the game object
    /// </summary>
    /// <returns></returns>
    public int getHealth()
    {
        return health;
    }

    /// <summary>
    /// Sets the health of the player
    /// </summary>
    /// <param name="h"></param>
    public void setHealth(int h)
    {
        health = h;
    }

    /// <summary>
    /// Reduces the health by a certain damage
    /// </summary>
    /// <param name="damage"></param>
    public void takeDamage(int damage)
    {
        // if the health is greater than zero, apply damage
        if (health > 0)
        {
            health = health - damage;
        }
        // if the health is less than or equal to zero, then destroy
        else
        {          
            Destory();
        }
    }
	
    /// <summary>
    /// Handle destroy/diable of gameobject
    /// </summary>
    void Destory()
    {
        // if the object with zero health is the player just disable
        if(gameObject.tag == "Player")
        {  
            this.gameObject.SetActive(false); 
        }
        // if the object with zero health is the enemy
        else
        {
            // increase money
            Wallet.money = Wallet.money + 2;
            // destroy enemy
            Destroy(this.gameObject);         
        }      
    }	
}
