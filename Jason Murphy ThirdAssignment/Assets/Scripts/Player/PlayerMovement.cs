﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour
{
    // speed of the player
    public float speed = 6f;
    // reference to the player's rigidbody
    Rigidbody playerRigidbody;
    // stores the player's movement vector
    Vector3 movement;

    void Start()
    {
        // get the players rigidbody
        playerRigidbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Allows the player to move
    /// </summary>
    /// <param name="h"></param>
    /// <param name="v"></param>
    public void Move(float h, float v)
    {
        // Set the movement vector based on the axis input.
        movement.Set(h, 0f, v);

        // Normalise the movement vector and make it proportional to the speed per second.
        movement = movement.normalized * speed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        
        playerRigidbody.MovePosition(transform.position + movement);
    }

    /// <summary>
    /// Allows the player to rotate
    /// </summary>
    /// <param name="turnDir"></param>
    public void Turning(Vector3 turnDir)
    {
        if (turnDir != Vector3.zero)
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = (transform.position + turnDir) - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);

         
            // Set the player's rotation to this new rotation.
            playerRigidbody.MoveRotation(newRotatation);
        }
    }
}

