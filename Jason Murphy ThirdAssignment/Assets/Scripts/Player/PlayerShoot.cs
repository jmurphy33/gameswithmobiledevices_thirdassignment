﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour
{
    // keep track of the current gun to shoot with
    public Gun weapon;
   
    // used to determine hits against enemies
    Ray shootRay;
    RaycastHit shootHit;

    // effects for gun, including sound, particals, bullet trail and muzzle flash
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;

    // layer mask used to only determine hits agains enemies
    public LayerMask mask;
    // time to display effects for 
    float effectsDisplayTime = 0.2f;
   
    // to determine time between shots
    float timer;
    public float timeBetweenBullets = 0.15f;

    void Start()
    {
        setComponents();
    }

    /// <summary>
    /// Resets the componets needed to shoot the gun
    /// </summary>
    public void setComponents()
    {
        // get effects on gun
        gunParticles = GetComponentInChildren<ParticleSystem>();
        gunLight = GetComponentInChildren<Light>();
        gunAudio = GetComponent<AudioSource>();
        gunLine = GetComponentInChildren<LineRenderer>();
        // get the current gun 
        weapon = GetComponentInChildren<Gun>();
        // ajust the colour of the effects
        SetColors(weapon.color);
    }


    /// <summary>
    /// Determines if the player can shoot
    /// </summary>
    /// <returns></returns>
    bool CanShoot()
    {
        // initially false, will test if can shoot
        bool isAbleToShoot = false;

        // if time is greater than the fire rate and the gun has ammo 
        if (timer > weapon.fireRate && weapon.ammo > 0)
        {
            // can shoot
            isAbleToShoot = true;
        }

        // return result
        return isAbleToShoot;
    }

    /// <summary>
    /// Calls shoot functionality if the player can shoot
    /// </summary>
    public void Shoot(bool shoot)
    {
        if (shoot)
        {
            // set shoot timer
            timer += Time.deltaTime;

            // if the player is allowed to fire 
            if (CanShoot())
            {
                // fire the gun
                Fire();
            }
            // if time is up
            if (timer >= timeBetweenBullets * effectsDisplayTime)
            {
                // turn off effects
                DisableEffects();
            }
        }
        else
        {
            DisableEffects();

        }
    }


    /// <summary>
    /// Handles shooting and hit functionality
    /// </summary>
    void Fire()
    {
        // reduce ammo in gun
        weapon.ammo = weapon.ammo - 1;
        // set timer of shoot time
        timer = 0f;
        // play gun sound 
        gunAudio.Play();
        // set light of muzzle flash
        gunLight.enabled = true;
        // turn off and on the muzzle flash particles
        gunParticles.Stop();
        gunParticles.Play();
        // enable bullet trail
        gunLine.enabled = true;
        
        // set position, direction and origin of bullet trail
        gunLine.SetPosition(0, weapon.spawn.transform.position);
        shootRay.origin = weapon.spawn.transform.position;
        shootRay.direction = weapon.spawn.transform.forward;

        // decide of raycast has hit an enemey
        if (Physics.Raycast(shootRay, out shootHit, weapon.range, mask))
        {
            // take health away from enemy
            shootHit.collider.GetComponent<HealthController>().takeDamage(weapon.damage);

            // set enemy to red to show hit
            shootHit.transform.GetComponent<Renderer>().material.color = Color.red;

            // set the end of the bullet trail where enemy was hit
            gunLine.SetPosition(1, shootHit.point);
        }
        
        else
        {
            // if nothing was hit, set end of bullet trail to where the collision occured
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * weapon.range);
        }
    }

    /// <summary>
    /// Reload functionality
    /// </summary>
    public void Reload()
    {
        // call the weapons reload, resetting the bullet count
        weapon.Reload();
    }

    /// <summary>
    /// turn off the effects of the gun
    /// </summary>
    public void DisableEffects()
    {
        // Disable the line renderer and the light.
        gunLine.enabled = false;
        gunLight.enabled = false;
    }

    /// <summary>
    /// Sets the colours of the gun effects
    /// </summary>
    /// <param name="color"></param>
    private void SetColors(Color color)
    {
        gunLine.material.color = color;
        gunLight.color = color;
        gunParticles.startColor = color;
    }

}

