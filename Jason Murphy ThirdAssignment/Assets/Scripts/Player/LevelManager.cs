﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    /// <summary>
    /// Loads the main menu scene
    /// </summary>
    public void LoadEndGame()
    {
        SceneManager.LoadScene("EndGame");
    }

    /// <summary>
    /// Loads the main game scene
    /// </summary>
    public void LoadNewGame()
    {
        SceneManager.LoadScene("s1");
    }

}
