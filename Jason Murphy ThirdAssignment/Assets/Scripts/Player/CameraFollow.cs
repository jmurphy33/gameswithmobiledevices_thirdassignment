﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    // reference to the player game object
    public GameObject target;
    // references the targets position during update
    private Vector3 cameraTarget;
    Quaternion rotation;

    void Awake()
    {
        // set rotation to current rotation of cam
        rotation = transform.rotation;
    }


    void Update()
    {
        // set camera target to new location of player
        cameraTarget = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
    }

    void LateUpdate()
    {
        // rotate by orignal position. Keeps the rotation of camera fixed in position
        transform.rotation = rotation;
        // set the position of the camera using lerp to the new position of the player
        transform.position = Vector3.Lerp(transform.position, cameraTarget, Time.deltaTime * 2);
    }
}