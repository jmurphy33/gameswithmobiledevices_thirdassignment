﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // reference the player shoot functionality
    PlayerShoot playerShoot;
    // reference the player movement functionality
    PlayerMovement playerMovement;
    // reference the player gun inventory functinality
    GunInventory gunInventory;
    // displays the players ammo 
    AmmoGUI ammoGui;
    // controls advertising options
    AdManager adManager;
    // 
    LevelManager levelManager;
    // number of times the player has died
    int livesCount;

    // if reload is pressed
    bool isPressed;

    // Use this for initialization
    void Awake()
    {
        // get the player's movment component 
        playerMovement = GetComponent<PlayerMovement>();
        // get the player's gun inventory component
        gunInventory = GetComponent<GunInventory>();

        // references the main object in the scene
        GameObject main = GameObject.FindGameObjectWithTag("Main");

        // gets the advertising manager from the main object
        adManager = main.GetComponent<AdManager>();
        // gets the level manager from the main object
        levelManager = main.GetComponent<LevelManager>();

        // gets the player shoot component
        playerShoot = GetComponentInChildren<PlayerShoot>();

        // gets the ammo displayer componet
        ammoGui = main.GetComponent<AmmoGUI>();

    }

    // Update is called once per frame
    void Update()
    {
        // get the axis of movment
        float h = CrossPlatformInputManager.GetAxisRaw("Horizontal");
        float v = CrossPlatformInputManager.GetAxisRaw("Vertical");

        // use movement component to move player
        playerMovement.Move(h, v);

        // get the axis of rotation
        Vector3 turnDir = new Vector3(CrossPlatformInputManager.GetAxisRaw("Mouse X"), 0f, CrossPlatformInputManager.GetAxisRaw("Mouse Y"));

        // use the movement component to rotate player
        playerMovement.Turning(turnDir);

        // if the player isn't turning don't shoot
        playerShoot.Shoot(turnDir != Vector3.zero);

    }

    /// <summary>
    /// Allows the player to switch guns
    /// </summary>
    /// <param name="gun"></param>
    public void SwitchWeapon(GameObject gun)
    {
        // using the passed in gun, switch gun in inventory
        gunInventory.SwitchGun(gun);
        // reset the shoot component
        playerShoot = GetComponentInChildren<PlayerShoot>();
        // reset the components within the shoot script
        playerShoot.setComponents();
        // reset ammo count
        ammoGui.setGun();
    }

    /// <summary>
    ///  turn off button press boolean
    /// </summary>
    public void buttonNotPressed()
    {
        isPressed = false;
    }

    /// <summary>
    /// turn on button press boolean
    /// </summary>
    public void buttonPressed()
    {
        isPressed = true;
    }

    /// <summary>
    /// When the player game object has been disabled
    /// </summary>
    void OnDisable()
    {
        // increase lives used by one
        livesCount = livesCount + 1;
        // show advertisment
        adManager.ShowAd(livesCount);

        // if lives used is 5
        if (livesCount == 5)
        {
            // reset lives
            livesCount = 0;
        }
        // load menu scene

        levelManager.LoadEndGame();
    }

    /// <summary>
    /// Used with reload button 
    /// </summary>
    public void reloadPressed()
    {
        // call reload functionality from player shoot component
        playerShoot.Reload();
        // adjust ammo text
        ammoGui.setGun();
    }

    Vector3 getMovementInput()
    {
        // get movement input from virtual joystick
        Vector3 move = new Vector3(CrossPlatformInputManager.GetAxisRaw("Horizontal"),
                            0,
                           CrossPlatformInputManager.GetAxisRaw("Vertical"));

        return move;
    }

    Vector3 getRotationInput()
    {
        // get rotation input from virtual joytick
        float aimx, aimy;
        aimx = CrossPlatformInputManager.GetAxis("Horizontal1");
        aimy = CrossPlatformInputManager.GetAxis("Vertical1");
        return new Vector3(aimx, aimy, 0);
    }

    /// <summary>
    /// Used for shoot functionality
    /// Checks if player is rotating
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    bool joyStickIsAiming(Vector3 input)
    {
        // if the virtual joystick is not 0,0,0 the player intends to shoot
        if (input != Vector3.zero)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
