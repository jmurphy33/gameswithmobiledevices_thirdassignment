﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RespawnController : MonoBehaviour {
    // reference to player 
    GUIController guiController;

    void Start()
    {
        // get the GUI controller
        guiController = GameObject.FindGameObjectWithTag("Main").GetComponent<GUIController>();
    }

    public void Respawn()
    {
        // reload the main scene
        SceneManager.LoadScene(0);
    }
}
