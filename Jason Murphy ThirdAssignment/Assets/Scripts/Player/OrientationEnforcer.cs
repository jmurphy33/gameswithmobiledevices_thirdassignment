﻿using UnityEngine;
using System.Collections;

public class OrientationEnforcer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // adjusts the orientation to landscape
        // used to keep virtual joy sticks axis correct
        if(Screen.orientation == ScreenOrientation.Portrait)
        {
            Screen.orientation = ScreenOrientation.LandscapeRight;
        }       
	}
	
    void Update()
    {
        // after joysticks have been setup correctly, return
        // orientation to auto
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

}
