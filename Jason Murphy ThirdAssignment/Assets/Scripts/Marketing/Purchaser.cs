﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener
{
    public Text M16ButtonText;
    // used to switch weapons on purchases
    private static PlayerController playerController;
    // used to alter health on purchases
    private static HealthController playerHealth;
    // stores current variables of a gun, such as cost
    private static Gun currentGunStats;
    // stores the current gun
    public static GameObject currentGun;

    // button for health, used to alter text (not used)
    public Button healthButton;
    // coins not used
    private static string kProductIDcoinsConsumable = "coins";
    private static string kProductNamecoinsConsumable = "coins";


    // store controller to handle purchases
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;
    // id for m16 gun product
    private static string kProductIDM16NonConsumable = "m16gun";
    // name for m16 gun to link with id
    private static string kProductNameGoogleM16NonConsumable = "m16gun";
    // id for health product
    private static string kProductIDHealthConsumable = "health";
    // name linked with id for health
    private static string kProductNameGoogleHealthConsumable = "health";

   
    // to check which products have been bought
    private static bool m16Bought, healthBought;


    void Start()
    {
        try
        {
            // set all products to not bought
            m16Bought = false;
            healthBought = false;

            // find the player
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            // get the player's health component
            playerHealth = player.GetComponent<HealthController>();

            // get the player's player controller componenet
            playerController = player.GetComponent<PlayerController>();

            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }

            // if not null, used when the player has died and respawned
            if (m_StoreController != null)
            {
                // set the products already bought
                sortProducts();
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier with its store-specific identifiers.
        builder.AddProduct(kProductIDM16NonConsumable, ProductType.NonConsumable, new IDs() { { kProductNameGoogleM16NonConsumable, GooglePlay.Name }, });
        builder.AddProduct(kProductIDHealthConsumable, ProductType.Consumable, new IDs() { { kProductNameGoogleHealthConsumable, GooglePlay.Name }, });
        builder.AddProduct(kProductIDcoinsConsumable, ProductType.Consumable, new IDs() { { kProductNamecoinsConsumable, GooglePlay.Name }, });
        UnityPurchasing.Initialize(this, builder);

    }

    /// <summary>
    /// Decides which products have been bought previously
    /// </summary>
    void sortProducts()
    {
        // check if the m16 gun has be bough
        Product m16gun = m_StoreController.products.WithID("m16gun");

        // if already has a receipt from a previous purchase
        if (m16gun != null && m16gun.hasReceipt)
        {
            // set to bought
            m16Bought = true;
            // change text to say use instead of buy
            GameObject.Find("m16ButtonText").GetComponent<Text>().text = "use";
        }
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    /// <summary>
    /// Allows the player to buy health through in app purchasing
    /// </summary>
    public void BuyHealth()
    {
        // buy health passing in the id 
        BuyProductID(kProductIDHealthConsumable);
    }

    /// <summary>
    /// Buy the M16 gun through in app purchasing
    /// </summary>
    /// <param name="gun"></param>
    public void BuyM16(GameObject gun)
    {
        // if not already bought
        if (!m16Bought)
        {
            // buy the product, passing in the id
            BuyProductID(kProductIDM16NonConsumable);
        }

        // if bought already
        if (m16Bought)
        {
            // set the text of the button to say use
            GameObject.Find("m16ButtonText").GetComponent<Text>().text = "use";
            // set the current stats of the gun 
            currentGunStats = gun.GetComponent<Gun>();
            // switch the current gun
            playerController.SwitchWeapon(gun);
        }
    }

    /// <summary>
    /// Allows the player to bug a gun using money they have earned
    /// within the game. Not bought with real money
    /// </summary>
    /// <param name="gun"></param>
    public void BuyGunWithInGameCurrency(GameObject gun)
    {
        // retrieve the variables linked with gun
        currentGunStats = gun.GetComponent<Gun>();

        // if the player has earned enough money within the game to purchase gun 
        if (Wallet.money >= currentGunStats.cost)
        {
            // change players gun to purchased gun
            playerController.SwitchWeapon(gun);
            // reduce money in wallet
            Wallet.money = Wallet.money - currentGunStats.cost;
        }
    }

    /// <summary>
    /// Used to initialize purchase by passing in product id
    /// </summary>
    /// <param name="productId"></param>
    void BuyProductID(string productId)
    {
        // If the stores throw an unexpected exception, use try..catch to protect logic here.
        try
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing system's products collection.
                Product product = m_StoreController.products.WithID(productId);
                
                // if product exists and is available to buy
                if (product != null && product.availableToPurchase)
                {
                    // start purchase passing in the product
                    m_StoreController.InitiatePurchase(product);
                }
            }
            else
            {
                // display not initialized 
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        // Complete the unexpected exception handling ...
        catch (Exception e)
        {
            // display error
            string message = "BuyProductID: FAIL. Exception during purchase. " + e;
            Debug.Log(message);
        }
    }


    /// <summary>
    ///  Called once the store cotroller has been initialized
    ///   
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="extensions"></param>
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
        // sort which products thae player has bought previously
        sortProducts();
    }

    /// <summary>
    /// Called if the store controller fails to initialize
    /// </summary>
    /// <param name="error"></param>
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason.
        string message = "OnInitializeFailed InitializationFailureReason:" + error;
        Debug.Log(message);
    }

    /// <summary>
    /// Used to determine if the player can buy a product
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        // if the product id is m16gun
        if (String.Equals(args.purchasedProduct.definition.id, kProductIDM16NonConsumable, StringComparison.Ordinal))
        {
            // set the gun to bought
            m16Bought = true;
        }

        // if the product id is health
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDHealthConsumable, StringComparison.Ordinal))
        {
            // increase the players health by 100
            playerHealth.setHealth(playerHealth.getHealth() + 100);
            // disallow the player from buying more health. Can only buy health once a game
            GameObject.Find("HealthButton").GetComponent<Button>().interactable = false;
        }
        return PurchaseProcessingResult.Complete;
    }


    /// <summary>
    /// Called if the purchase of a produce is unsuccessful
    /// </summary>
    /// <param name="product"></param>
    /// <param name="failureReason"></param>
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // display error
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

}
