﻿using UnityEngine;
using System.Collections;

public class ShopController : MonoBehaviour {

    // stores reference to the shop canvas to display products
    public Canvas shopCanvas;

    /// <summary>
    /// Opens the shop canvas and pauses the game
    /// </summary>
    public void OpenShop()
    {
        Time.timeScale = 0;
        shopCanvas.enabled = true;
       
    }

    /// <summary>
    /// Closes the shop canvas and unpauses the game
    /// </summary>
    public void CloseShop()
    {
        Time.timeScale = 1;
        shopCanvas.enabled = false;
        
    }
}

