﻿using UnityEngine;
using System.Collections;

public class ShopInventory : MonoBehaviour
{
    // (class not used)

    // an array of guns in the shop
    public GameObject[] Guns;
    Gun gun;

    
    /// <summary>
    /// Gets a gun from the gun array
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    public GameObject GetWeapon(int num)
    {
        return Guns[num];
    }
}
