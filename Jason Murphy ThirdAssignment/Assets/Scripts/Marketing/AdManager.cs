﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{


    public void ShowAd(int livesUsed)
    {
        // the player has died twice
        if (livesUsed == 1)
        {
            // and if the advertisment is ready
            if (Advertisement.IsReady())
            {
                // display advertisment 
                Advertisement.Show();
            }
        }
    }

    public void ShowAd(string zone = "rewardedVideo")
    {
        ShowOptions options = new ShowOptions();
        // setting the callback method to handle if the player watched the video or not
        options.resultCallback = AdCallbackhandler;

        // if the reward video is ready
        if (Advertisement.IsReady(zone))
        {
            // show reward ad
            Advertisement.Show(zone, options);
        }
    }

    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            // if the player whated the video to the end increase money by 5
            case ShowResult.Finished:
                Debug.Log("Ad Finished. Rewarding player...");
                Wallet.money = Wallet.money + 5;
                break;
            // if the player skip the ad don't reward them
            case ShowResult.Skipped:
                Debug.Log("Ad Skipped");
                break;
            // if the add failed display in debug
            case ShowResult.Failed:
                Debug.Log("Ad failed");
                break;
        }
    }


}
