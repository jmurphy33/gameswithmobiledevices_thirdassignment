﻿using UnityEngine;
using System.Collections;

public class Wallet : MonoBehaviour {
    // keeps track of the money of the player
    public static float money;
    // reference to the purchaser (not used)
    public Purchaser purchase;

    void Start()
    {
        // reset money
        money = 0;
    }	
}
